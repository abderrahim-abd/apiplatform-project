<?php

namespace App\EntityListener;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Bundle\SecurityBundle\Security;
uSE Symfony\Component\String\Slugger\SluggerInterface;
use Doctrine\ORM\Events;

#[AsEntityListener(Events::prePersist, entity: article::class)]
#[AsEntityListener(Events::preUpdate, entity: article::class)]
class ArticleEntityListner
{
    public function __construct(
        private SluggerInterface $slugger,
        private Security $security )
    {

    }

    public function prePersist(Article $article, LifecycleEventArgs $event): void
    {
        $article->computeSlug($this->slugger);
        $article->setUser($this->security->getUser());
    }

    public function preUpdate(article $article, LifecycleEventArgs $event): void
    {
        $article->computeSlug($this->slugger);
    }
}